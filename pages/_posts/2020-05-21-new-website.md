---
title: New website
date: 2020-09-01
---

Yippie! We have a new website. It is static generated and anyone can contribute by issuing a git pull request. See [[https://gitlab.com/calyxos/calyxos.org]].

In the coming weeks, we will be adding help documentation, tutorials, release change logs, and more. Stay tuned!
