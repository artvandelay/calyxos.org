---
title: About CalyxOs
nav_title: About
---

Everyone needs a phone. Not everyone wants governments and corporations tracking their every move and spying on their communications.  CalyxOS lets you have your cake and eat it too, with  "Privacy by Design"

In social science, agency is defined as: the capacity of individuals to act independently and to make their own free choices.  There is a whole community of free software developers working to build tools that enable us to take back our privacy, and we have curated a collection of the "best of the best" in order to give you back your agency.

When you use a phone with CalyxOS:

* you are empowered by the combined expertise of the Internet privacy community
* phone calls are encrypted so nobody can listen in
* text messages are encrypted and a timer can be set so they disappear
* web browsing is anonymized
* advertising trackers are blocked
* built-in free "Virtual Private Network" services protect you from being spied on
* your phone is receiving regular, timely, automatic security updates
* your data is backed up with strong encryption to your personal cloud server or to USB storage

# Who is it for

CalyxOS is for you.

CalyxOS minimizes the tracking, surveillance, and spying done by phone manufacturers, mobile phone service providers, internet service providers, advertising companies, data miners, and malicious hackers. The operating system is designed to ensure maximum usability and flexibility, so that you have an array of choices available to ensure your security and privacy.

CalyxOS is designed with the needs of human rights frontline defenders, journalists, lawyers, and political and social activist groups in mind.
